package com.aws.devicefarm.webinar.nativeapp;


	import org.testng.annotations.Test;

	import io.appium.java_client.AppiumDriver;
	import io.appium.java_client.android.AndroidDriver;

	import java.io.File;
	import java.net.URL;
	import org.openqa.selenium.By;
	import org.openqa.selenium.OutputType;
	import org.openqa.selenium.TakesScreenshot;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.remote.DesiredCapabilities;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.AfterMethod;
	import org.testng.annotations.BeforeMethod;
	import io.appium.java_client.remote.MobileCapabilityType;

	public class Nativeapp {

		private static AppiumDriver<WebElement> driver = null;
		
		public boolean takeScreenshot(final String name) {
		    String screenshotDirectory = System.getProperty("appium.screenshots.dir", System.getProperty("java.io.tmpdir", ""));
		    File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		    return screenshot.renameTo(new File(screenshotDirectory, String.format("%s.png", name)));
		}
		
	      
	    @BeforeMethod
	    public void setUp() throws Exception {
	        DesiredCapabilities dc = new DesiredCapabilities();
	        //local setup
	        
	        dc.setCapability("platformVersion", "4.4");
	        dc.setCapability("platformName", "Android");
	        dc.setCapability("deviceName", "Test");
	        //device farm and universal setup
	            URL url = new URL("http://127.0.0.1:4723/wd/hub");
	            driver = new AndroidDriver(url, dc);
	    }
	     
	     
	    @Test
	    public void test1() throws InterruptedException {
	    	
	    	//make the driver wait until the Splash Activity is gone and to the Main Activity be clickable
	    	WebDriverWait wait = new WebDriverWait(driver, 300, 300);
	    	wait.until(ExpectedConditions.elementToBeClickable(By.name("User Data Storage")));
	    	Thread.sleep(10000);
	    	String screenshot1 = null;
	        takeScreenshot(screenshot1);
	    	
	    }
	    	
	    @Test
	 	public void test2() throws InterruptedException {	
	    	
	    	WebDriverWait wait = new WebDriverWait(driver, 300, 300);
	    	wait.until(ExpectedConditions.elementToBeClickable(By.name("User Data Storage")));
	    	
	        //click on the element named User Data Storage
	    	driver.findElementByName("User Data Storage").click();
	    	
	    	Thread.sleep(10000);
	    	
	    	//take screenshot
	    	String screenshot2 = null;
	        takeScreenshot(screenshot2);
	    	
	    }
	    
	    @Test
	    public void test3() throws InterruptedException {
	    	
	    	//make the driver wait until the Splash Activity is gone and to the Main Activity be clickable
	    	WebDriverWait wait = new WebDriverWait(driver, 300, 300);
	    	wait.until(ExpectedConditions.elementToBeClickable(By.name("User Data Storage")));
	    	
	        //click on the element named User Data Storage
	    	driver.findElementByName("User Data Storage").click();
	    
	    	//click on the Demo User File Storage item
	    	driver.findElementByName("Demo User File Storage").click();
	    	
	    	Thread.sleep(10000);
	    	
	    	
	    	//take screenshot
	    	String screenshot3 = null;
	        takeScreenshot(screenshot3);
	    	
	    	
	    	 }
	    @Test
	 	public void test4() throws InterruptedException {
	    	
	    	//make the driver wait until the Splash Activity is gone and to the Main Activity be clickable
	    	WebDriverWait wait = new WebDriverWait(driver, 300, 300);
	    	wait.until(ExpectedConditions.elementToBeClickable(By.name("User Data Storage")));
	    	
	        //click on the element named User Data Storage
	    	driver.findElementByName("User Data Storage").click();
	    
	    	//click on the Demo User File Storage item
	    	driver.findElementByName("Demo User File Storage").click();
	    	
	    	//click on the public folder
	    	driver.findElementById("com.amazon.mysampleapp:id/button_userFiles_public").click();
	    	
	    	Thread.sleep(10000);
	    	
	    	
	    	//take screenshot
	    	String screenshot4 = null;
	        takeScreenshot(screenshot4);
	    	
	    	
	    		 
	    	 }
	    
	    
	    @Test
	    public void test5() throws InterruptedException {
	    	
	    	//make the driver wait until the Splash Activity is gone and to the Main Activity be clickable
	    	WebDriverWait wait = new WebDriverWait(driver, 300, 300);
	    	wait.until(ExpectedConditions.elementToBeClickable(By.name("User Data Storage")));
	    	
	        //click on the element named User Data Storage
	    	driver.findElementByName("User Data Storage").click();
	    
	    	//click on the Demo User File Storage item
	    	driver.findElementByName("Demo User File Storage").click();
	    	
	    	//click on the public folder
	    	driver.findElementById("com.amazon.mysampleapp:id/button_userFiles_public").click();
	    
	    	//make the driver wait for the s3 to load all the files from the specific bucket and then click on the options button
	    	wait.until(ExpectedConditions.elementToBeClickable(By.id("com.amazon.mysampleapp:id/toolbar")));
	    	driver.findElementByAccessibilityId("More options").click();
	    	
	    	
	    	//take screenshot
	    	String screenshot5 = null;
	        takeScreenshot(screenshot5);
	    	
	    	
	    }
	    
	    @Test
	    public void test6() throws InterruptedException {
	    	
	     	//make the driver wait until the Splash Activity is gone and to the Main Activity be clickable
	    	WebDriverWait wait = new WebDriverWait(driver, 300, 300);
	    	wait.until(ExpectedConditions.elementToBeClickable(By.name("User Data Storage")));
	    	
	        //click on the element named User Data Storage
	    	driver.findElementByName("User Data Storage").click();
	    
	    	//click on the Demo User File Storage item
	    	driver.findElementByName("Demo User File Storage").click();
	    	
	    	
	    	
	    	//click on the public folder
	    	driver.findElementById("com.amazon.mysampleapp:id/button_userFiles_public").click();
	    
	    	//make the driver wait for the s3 to load all the files from the specific bucket and then click on the options button
	    	wait.until(ExpectedConditions.elementToBeClickable(By.id("com.amazon.mysampleapp:id/toolbar")));
	    	driver.findElementByAccessibilityId("More options").click();
	    	
	    	
	    	//click on the new folder option
	    	driver.findElementByName("New Folder").click();

	    	
	    	//write the name "Test Folder" on the new folder input
	    	driver.findElementByClassName("android.widget.EditText").sendKeys("Test Folder");
	    	
	    	//take screenshot
	    	
	        String screenshot6 = null;
	        takeScreenshot(screenshot6); 

	    	
	    }
	    
	    @Test
	    public void test7() throws InterruptedException {
	    	
	       	//make the driver wait until the Splash Activity is gone and to the Main Activity be clickable
	    	WebDriverWait wait = new WebDriverWait(driver, 300, 300);
	    	wait.until(ExpectedConditions.elementToBeClickable(By.name("User Data Storage")));
	    	
	        //click on the element named User Data Storage
	    	driver.findElementByName("User Data Storage").click();
	    
	    	//click on the Demo User File Storage item
	    	driver.findElementByName("Demo User File Storage").click();
	    	
	    	
	    	
	    	//click on the public folder
	    	driver.findElementById("com.amazon.mysampleapp:id/button_userFiles_public").click();
	    
	    	//make the driver wait for the s3 to load all the files from the specific bucket and then click on the options button
	    	wait.until(ExpectedConditions.elementToBeClickable(By.id("com.amazon.mysampleapp:id/toolbar")));
	    	driver.findElementByAccessibilityId("More options").click();
	    	
	    	
	    	//click on the new folder option
	    	driver.findElementByName("New Folder").click();

	    	
	    	//write the name "Test Folder" on the new folder input
	    	driver.findElementByClassName("android.widget.EditText").sendKeys("Test Folder");
	    	
	    	//find the element by id, and click on the "OK" option
	    	driver.findElementById("android:id/button1").click();
	    	
	    	//take screenshot
	    	String screenshot7 = null;
	        takeScreenshot(screenshot7);
	    	
	    	
	    }
	    
	    @Test
	    public void test8() throws InterruptedException {
	    	
	    	//make the driver wait until the Splash Activity is gone and to the Main Activity be clickable
	    	WebDriverWait wait = new WebDriverWait(driver, 300, 300);
	    	wait.until(ExpectedConditions.elementToBeClickable(By.name("User Data Storage")));
	    	
	        //click on the element named User Data Storage
	    	driver.findElementByName("User Data Storage").click();
	    
	    	//click on the Demo User File Storage item
	    	driver.findElementByName("Demo User File Storage").click();
	    	
	    	
	    	
	    	//click on the public folder
	    	driver.findElementById("com.amazon.mysampleapp:id/button_userFiles_public").click();
	    
	    	//make the driver wait for the s3 to load all the files from the specific bucket and then click on the options button
	    	wait.until(ExpectedConditions.elementToBeClickable(By.id("com.amazon.mysampleapp:id/toolbar")));
	    	driver.findElementByAccessibilityId("More options").click();
	    	
	    	//take screenshot
	    	String screenshot8 = null;
	        takeScreenshot(screenshot8);
	    	
	    		    }
	    
	    
	    	
	     
	    @AfterMethod
	    public static void tearDownClass() {
	                if (driver != null) {
	                    driver.quit();
	                }
	        }
	}
		
		



