package com.aws.devicefarm.webinar.webandroid;

import java.net.MalformedURLException;
import java.net.URL;
import java.io.File;
import java.lang.*;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.*;
 
public class WebApp
{
	
	public boolean takeScreenshot(final String name) {
	       String screenshotDirectory = System.getProperty("appium.screenshots.dir", System.getProperty("java.io.tmpdir", ""));
	       File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	       return screenshot.renameTo(new File(screenshotDirectory, String.format("%s.png", name)));
	   }
	
	private static WebDriver driver;
	
	
	@BeforeMethod
		public void setUp() throws Exception {
		    DesiredCapabilities capabilities = new DesiredCapabilities();
		    
		    //very important capability! it change the command timeout in order to make appium wait for any load problem
		    //like slow connection with internet or something and makes the test continue
		    capabilities.setCapability("newCommandTimeout", 600);
		    driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		    
			//go to the website
			driver.get("http://amazon.com.br/");
		
	        
	}
	
	@Test
		public void test1() throws MalformedURLException, InterruptedException{
		
		
			driver.findElement(By.name("k")).sendKeys("Kindle Paperwhite");
			
			String screenshot1 = null;
			takeScreenshot(screenshot1);

			driver.findElement(By.cssSelector("div.nav-search-submit")).click();
			Thread.sleep(10000);

			String screenshot2 = null;
			takeScreenshot(screenshot2);
			
			String conteudo = driver.findElement(By.xpath("//div[@class='sx-sparkle-text']/span[contains(text(), 'Kindle Paperwhite')]")).getText();
			Assert.assertEquals(conteudo, "Kindle Paperwhite", "Erro!");



		
	}

	@AfterMethod
	public static void tearDownClass() {
		 if (driver != null) {
		      driver.quit();
		          }
		 }
}