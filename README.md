# DeviceFarmScripts
Repositório de scripts de teste para o AWS Device Farm em Java (TestNG) e Python (unittest)

Preparação de ambiente para os testes e mais informações sobre o AWS Device Farm disponíveis em: http://docs.aws.amazon.com/es_es/devicefarm/latest/developerguide/welcome.html
