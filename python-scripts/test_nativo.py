from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
import unittest
import os

class CreateFolder(unittest.TestCase):
    def setUp(self):

        desired_caps = {}
        desired_caps['newCommandTimeout'] = 600
        # Returns abs path relative to this file and not cwd
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def tearDown(self):
        "Tear down the test"
        self.driver.quit()

    def test_createfolder(self):

        screenshot_folder = os.getenv('SCREENSHOT_PATH', '')

        wait = WebDriverWait(self.driver, 30)
        wait.until(EC.element_to_be_clickable((By.NAME,"User Data Storage")))
        self.driver.save_screenshot(screenshot_folder + "/screenshot1.png")

        self.driver.find_element_by_name("User Data Storage").click()
        sleep(10)
        self.driver.save_screenshot(screenshot_folder + "/screenshot2.png")

        self.driver.find_element_by_name("Demo User File Storage").click()
        self.driver.save_screenshot(screenshot_folder + "/screenshot3.png")

        self.driver.find_element_by_id("com.amazon.mysampleapp:id/button_userFiles_public").click()
        sleep(20)
        self.driver.save_screenshot(screenshot_folder + "/screenshot4.png")

        wait.until(EC.element_to_be_clickable((By.CLASS_NAME,"android.widget.ImageView")))
        self.driver.find_element_by_class_name("android.widget.ImageView").click()
        self.driver.save_screenshot(screenshot_folder + "/screenshot5.png")

        self.driver.find_element_by_name("New Folder").click()
        self.driver.find_element_by_class_name("android.widget.EditText").send_keys("Folder 1")
        self.driver.save_screenshot(screenshot_folder + "/screenshot6.png")

        self.driver.find_element_by_name("OK").click()
        sleep(20)
        self.driver.save_screenshot(screenshot_folder + "/screenshot7.png")



if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CreateFolder)
    unittest.TextTestRunner(verbosity=2).run(suite)
