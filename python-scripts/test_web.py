#!/usr/bin/python
# -*- coding: utf-8 -*-
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
import unittest
import os


class WebTest(unittest.TestCase):
	def setUp(self):
		desired_caps = {}
		#desired_caps["deviceName"] = "Test3"
		#desired_caps["platformName"] = "Android"
		#desired_caps["browserName"] = "Browser"
		desired_caps["newCommandTimeout"] = 600
		self.driver = webdriver.Remote(
			'http://127.0.0.1:4723/wd/hub', desired_caps)
	        self.driver.get ('https://amazon.com.br')

	def tearDown(self):
		self.driver.quit()
#

	def test_Kindle(self):
		sleep(10)
		screenshot_folder = os.getenv('SCREENSHOT_PATH', '/tmp')
		self.driver.save_screenshot(screenshot_folder + '/devicefarm1.png')
		wait = WebDriverWait(self.driver, 30)
		wait.until(EC.element_to_be_clickable((By.XPATH,"//div[@class='nav-search-field']/input[@id='nav-search-keywords']")))
		self.driver.find_element_by_xpath("//div[@class='nav-search-field']/input[@id='nav-search-keywords']").send_keys("Kindle Paperwhite")

		self.driver.save_screenshot(screenshot_folder + '/devicefarm2.png')

		wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,"div.nav-search-submit")))

		self.driver.find_element_by_css_selector("div.nav-search-submit").click()
		self.driver.save_screenshot(screenshot_folder + '/devicefarm3.png')

		sleep(50)

		conteudo = self.driver.find_element_by_xpath("//div[@class='sx-sparkle-text']/span[contains(text(), 'Kindle Paperwhite')]").text
		self.assertEqual(conteudo, "Krindle Paperwhite", "Não batem os nomes!")

if __name__ == '__main__':
	suite = unittest.TestLoader().loadTestsFromTestCase(WebTest)
	unittest.TextTestRunner(verbosity=2).run(suite)
