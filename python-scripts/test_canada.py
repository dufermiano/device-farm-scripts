#!/usr/bin/python
# -*- coding: utf-8 -*-
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
import unittest
import os


class BankOfCanada(unittest.TestCase):
    def setUp(self):
        desired_caps = {}
        desired_caps["deviceName"] = "Teste"
        desired_caps["platformName"] = "Android"
        desired_caps["browserName"] = "Browser"
        desired_caps["newCommandTimeout"] = 600
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        self.driver.get ('http://ec2-52-203-39-178.compute-1.amazonaws.com')

    def tearDown(self):
        self.driver.quit()
#

    def test_BankOfCanada(self):
        #screenshot_folder = os.getenv('SCREENSHOT_PATH', '/tmp')
        #self.driver.save_screenshot(screenshot_folder + '/devicefarm1.png')
        self.driver.execute_script("window.scrollBy(0, 3200);")
        #sleep(5)
        self.driver.find_element_by_xpath("//*[@id='main-content']/div[2]/div[3]/div/div/div/h2/a").click()

        #sleep(10)

        #self.driver.save_screenshot(screenshot_folder + '/devicefarm2.png')

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(BankOfCanada)
    unittest.TextTestRunner(verbosity=2).run(suite)
